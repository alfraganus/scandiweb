
<?php
Class Product {

    public $servername = "localhost";
    public $username = "veraluz_user";
    public $password = "m;MyEGskZ~]G";
    public $dbName = 'veraluz_scan';
    private $conn;

    public function __construct()
    {
        $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbName", $this->username, $this->password);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function insertDB($sku, $price, $name, $product_type, $product_feature)
    {
        try {
            $sql = "INSERT INTO products (sku, price, name,product_type,product_feature)
                VALUES ('$sku', '$price','$name','$product_type','$product_feature')";
            $this->conn->exec($sql);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        $this->conn = null;
    }

    public function getProducts()
    {
        try {
            $stmt = $this->conn->prepare("SELECT id, sku, price, name, product_type, product_feature FROM products");
            $stmt->execute();
            return $stmt->fetchAll();

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        $this->conn = null;
    }

    public function deleteProducts($product_ids)
    {
        try {
            foreach($product_ids as $id){
                $sql = "DELETE FROM products WHERE id=$id";
                $this->conn->exec($sql);
            }
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
        $conn = null;
    }


    public function redirect($filename)
    {
        if (!headers_sent())
            header('Location: ' . $filename);
        else {
            echo '<script type="text/javascript">';
            echo 'window.location.href="' . $filename . '";';
            echo '</script>';
            echo '<noscript>';
            echo '<meta http-equiv="refresh" content="0;url=' . $filename . '" />';
            echo '</noscript>';
        }
    }

    public function getProductType($category,$data)
    {
        $type='';
        $value='';
        switch ($category) {
            case 'book':
                $type ='Weight';
                $value =$data;
                break;
            case 'dvd':
                $type ='Size';
                $value = $data;
                break;
            case 'furniture':
                $type ='Dimention';
                $json = json_decode($data);
                $value = $json->height.' x '.$json->width.' x '.$json->length ;
                break;
        }
        return [
            'type'=>$type,
            'value'=>$value
        ];
    }

    public function uniqueNameGenerator($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}