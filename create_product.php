<?php
include "core_functions.php";
$product = new Product();
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <h2 class="text-center">Test task</h2>
    <div style="background: #eeeeee" class="alert alert-danger" id="show_error" role="alert">
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-12 pb-5">
            <!--Form with header-->
            <form action="<?= $_SERVER['PHP_SELF'];?>" method="POST">

                <div class="card border-primary rounded-0">
                    <div class="card-header p-0">
                        <div class="bg-info text-white text-center py-2">
                            <h3><i class="fa fa-envelope"></i> Add new product</h3>
                        </div>
                    </div>
                    <div class="card-body p-3">

                        <!--Body-->
                        <div class="form-group">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-user text-info"></i>SKU</div>
                                </div>
                                <input type="text" class="form-control" id="sku" name="sku" value="<?=$product->uniqueNameGenerator()?>" placeholder="sku" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-envelope text-info"></i>Name</div>
                                </div>
                                <input type="text" class="form-control" id="name" name="name" placeholder="name" >
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-envelope text-info"></i>Price</div>
                                </div>
                                <input type="text" class="form-control" id="price" name="price" placeholder="Price" >
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-envelope text-info"></i>Product type</div>
                                </div>
                                <select class="form-control" id="product_type" name="product_type" >
                                    <option>Please select the product type</option>
                                    <option value="dvd">DVD-disc</option>
                                    <option value="book">Book</option>
                                    <option value="furniture">Furniture</option>

                                </select>
                            </div>
                        </div>
                        <?php include 'inputs.php'?>

                        <div class="text-center">
                            <div class="row">
                                <div class="col-lg-6 pb-5">
                                    <input type="submit" name="submit_product" id="product_submit" value="Insert product" class="btn btn-info btn-block rounded-0 py-2">
                                </div>
                                <div class="col-lg-6 pb-5">
                                    <a href="index.php"><button type="button" class="btn btn-danger btn-block rounded-0 py-2">Back</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!--Form with header-->
        </div>
    </div>
</div>

<?php
if(isset($_POST['submit_product']))
{

    $sku=$_POST['sku'];
    $name=$_POST['name'];
    $price=$_POST['price'];
    $feature=null;
    $product_type=$_POST['product_type'];

    switch ($product_type) {
        case 'dvd':
            $feature=$_POST['size'];
            break;
        case 'furniture':
            $height=$_POST['height'];
            $width=$_POST['width'];
            $length=$_POST['length'];
            $bookArray = array(
                'height'=>$height,
                'width'=>$width,
                'length'=>$length
            );
            $feature = json_encode($bookArray);
            break;
        case 'book':
            $feature=$_POST['weight'];
            break;
    }

    $product->insertDB($sku,$price,$name,$product_type,$feature);
    $product->redirect('index.php');
}
?>
<script src="main.js"></script>