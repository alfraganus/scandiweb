<?php include 'core_functions.php' ?>
<!DOCTYPE html>
<html>
<head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="box">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">

                </ul>
                <form class="form-inline my-2 my-lg-0" action="<?= $_SERVER['PHP_SELF'];?>" method="POST">
                    <a href="create_product.php">
                        <button class="btn btn-outline-success my-2 my-sm-0 mr-2" type="button">Add</button>
                    </a>
                    <input type="submit" name="submit_product" id="product_delete" value="Mass delete" class="btn btn-danger  rounded-0 py-2">
            </div>
        </nav>

        <div class="row">
            <?php
            $productClass = new Product();
            $getProducts = $productClass->getProducts();
            foreach ($getProducts as $product):
             $productType = $productClass->getProductType($product['product_type'],$product['product_feature']);
                ?>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="box-part text-center">
                        <input name="delete[]" value="<?=$product['id']?>" type="checkbox" class="pro_delete_btn" style="float:left">
                        <i class="fa fa-instagram fa-3x" aria-hidden="true"></i>

                        <div class="title">
                            <h4><?=$product['sku']?></h4>
                        </div>
                        <div class="title">
                            <h4><?=$product['name']?></h4>
                        </div>
                        <div class="title">
                            <h5>$<?=$product['price']?></h5>
                        </div>

                        <div class="title">
                            <h5><?=$productType['type']?>: <?=$productType['value']?></h5>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
    </form>
</div>
<?php
if(isset($_POST['delete'])) {
    $product_ids = $_POST['delete'];
    $productClass->deleteProducts($product_ids);
    echo "<script>alert('Product(s) deleted successfully!')</script>";
    $productClass->redirect('/');
}
?>
</body>
</html>