<div id="dvd">
    <h3 class="text-center">Please, provide size</h3>
    <div class="form-group">
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-envelope text-info"></i>Size</div>
            </div>
            <input type="text" class="form-control" id="size" name="size" placeholder="Product type" >
        </div>
    </div>
</div>


<div id="furniture">
    <h3 class="text-center">Please, provide dimensions</h3>
    <div class="form-group">
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-envelope text-info"></i>Height (cm)</div>
            </div>
            <input type="text" class="form-control" id="height" name="height" placeholder="Height (cm)" >
        </div>
    </div>

    <div class="form-group">
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-envelope text-info"></i>Width (cm)</div>
            </div>
            <input type="text" class="form-control" id="width" name="width" placeholder="Width (cm)" >
        </div>
    </div>

    <div class="form-group">
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-envelope text-info"></i>Length (cm)</div>
            </div>
            <input type="text" class="form-control" id="length" name="length" placeholder="Length (cm)" >
        </div>
    </div>
</div>

<div id="book">
    <h3 class="text-center">Please, provide weight</h3>
    <div class="form-group">
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-envelope text-info"></i>Weight (kg)</div>
            </div>
            <input type="text" class="form-control" id="weight" name="weight" placeholder="Weight (kg)" >
        </div>
    </div>
</div>


<style>
    #book, #furniture, #dvd {
        display: none;
    }
</style>