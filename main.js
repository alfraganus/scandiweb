
$("#product_type").on('change', function() {
    console.log($(this).val());
   var product_type = $(this).val();
   var dvd = $('#dvd');
   var furniture = $('#furniture');
   var book = $('#book');

    switch(product_type) {
        case 'dvd':
            dvd.show();
            furniture.hide();
            book.hide();
            break;
        case 'furniture':
            furniture.show();
            dvd.hide();
            book.hide();
            break;
        case 'book':
            book.show();
            dvd.hide();
            furniture.hide();
            break;

        default:
            book.hide();
            dvd.hide();
            furniture.hide();
    }
});

$("#product_submit").on('click', function(e) {
    var skuValue = $('#sku').val();
    var nameValue = $('#name').val();
    var priceValue = $('#price').val();
    var dvdValue = $('#size').val();
    var bookValue = $('#weight').val();
    var heightValue = $('#height').val();
    var widthValue = $('#width').val();
    var lengthValue = $('#length').val();

    var error = $('#show_error');
    var product_type = $('#product_type').val();
    var error_message = "Please, submit required data";
    if(product_type ==='dvd')
    {
        if(dvdValue=='' || dvdValue==undefined) {
            e.preventDefault();
            error.text(error_message);
        }
    } else if(product_type ==='book')
    {
        if(bookValue=='' || bookValue==undefined) {
            e.preventDefault();
            error.text(error_message);
        }
    }else if(product_type ==='furniture')
    {
        if(heightValue=='' || widthValue=='' || lengthValue=='') {
            e.preventDefault();
            error.text(error_message);
        }
    }else if(skuValue =='' || nameValue=='' || priceValue=='' || product_type=='Please select the product type')
    {
        e.preventDefault();
        error.text(error_message);
    }

    if(!$.isNumeric(priceValue)) {
        e.preventDefault();
        error.text('Please, provide the data of indicated type for price');
    }

});


$("#product_delete").on('click', function(e) {
    if(!$('.pro_delete_btn').is(':checked')) {
        e.preventDefault();
    }
});